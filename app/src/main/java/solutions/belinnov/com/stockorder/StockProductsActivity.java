package solutions.belinnov.com.stockorder;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class StockProductsActivity extends AppCompatActivity {

    ListView stock_lv;
    ProgressBar spinner;
    StockProductListAdapter stockProductListAdapter;
    RestInterface restInterface;
    RestClient restClient;
    AppSession mAppSession;

    EditText searchEditText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_products);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        restClient = new RestClient();
        restInterface = restClient.getApiService();
        mAppSession = AppSession.getInstance();

        stock_lv = (ListView) findViewById(R.id.stock_product_lv);
        spinner = (ProgressBar) findViewById(R.id.stock_layout_progressBar);
        initStockProduct();


        searchEditText= (EditText) findViewById(R.id.searchEditText);
        searchEditText.requestFocus();


        // Add Text Change Listener to EditText
        searchEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Call back the Adapter with current character to Filter
                stockProductListAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });



    }

    private void initStockProduct() {
        int TemplateId =74;
        spinner.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        restInterface.getStockOrder(TemplateId, new Callback<List<StockOrder>>() {
            @Override
            public void success(List<StockOrder> stockOrders, Response response) {
                spinner.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                mAppSession.setStockOrders(stockOrders);
                stockProductListAdapter = new StockProductListAdapter(StockProductsActivity.this, mAppSession.getStockOrders());
                stock_lv.setAdapter(stockProductListAdapter);
            }

            @Override
            public void failure(RetrofitError error) {
                spinner.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                displayAlert(getResources().getString(R.string.server_error_message));
            }
        });

    }





    private void displayAlert(String message) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(StockProductsActivity.this);
        // Setting Dialog Title
        alertDialog.setTitle(getResources().getString(R.string.app_name));
        // Setting Dialog Message
        alertDialog
                .setMessage(message);
        // On pressing Settings button
        alertDialog.setPositiveButton(getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        /*finish();
                        System.exit(0);*/
                    }
                });
        // Showing Alert Message
        alertDialog.show();
    }
}
