package solutions.belinnov.com.stockorder;




import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Gaurav on 6/10/2016.
 */
public class AppSession {

    private static AppSession instance;
    public static synchronized AppSession getInstance() {
        if (instance == null) {
            instance = new AppSession();
        }
        return instance;
    }
    public static void setInstance(AppSession instance) {
        AppSession.instance = instance;
    }


    public List<StockOrder> stockOrders;
    public List<StockTemplate> stockProducts;



    public List<StockOrder> getStockOrders() {
        return stockOrders;
    }

    public void setStockOrders(List<StockOrder> stockOrders) {
        this.stockOrders = stockOrders;
    }


    public List<StockTemplate> getStockProducts() {
        return stockProducts;
    }

    public void setStockProducts(List<StockTemplate> stockProducts) {
        this.stockProducts = stockProducts;
    }
}
