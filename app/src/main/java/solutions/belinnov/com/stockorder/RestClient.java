package solutions.belinnov.com.stockorder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by Gaurav on 6/10/2016.
 */
public class RestClient {
   //private static final String API_URL = "http://a77f48b2.ngrok.io/TCBROMS";
   //private static final String API_URL = "http://app.simplexservices.co.uk";
   private static final String API_URL = "http://13.88.29.130/tcbroms";
   private final RestInterface apiService;

    public RestClient() {
        Gson gson = new GsonBuilder()
                .create();
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(API_URL)
                .setClient(new OkClient())
                .setConverter(new GsonConverter(gson))
                .build();
        apiService = restAdapter.create(RestInterface.class);
    }

    public RestInterface getApiService() {
        return apiService;
    }

}
