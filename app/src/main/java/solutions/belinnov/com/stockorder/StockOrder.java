package solutions.belinnov.com.stockorder;

/**
 * Created by belinnove solution on 01/05/2018.
 */

public class StockOrder {

    public int ProductID;
    public String ProductName;
    public float StockQuantity;

    public int getProductID() {
        return ProductID;
    }

    public void setProductID(int productID) {
        ProductID = productID;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public float getStockQuantity() {
        return StockQuantity;
    }

    public void setStockQuantity(float stockQuantity) {
        StockQuantity = stockQuantity;
    }


}
