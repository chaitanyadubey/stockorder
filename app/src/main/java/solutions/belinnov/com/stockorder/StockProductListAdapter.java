package solutions.belinnov.com.stockorder;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gaurav on 6/10/2016.
 */
public class StockProductListAdapter extends BaseAdapter implements Filterable {

    Context mContext;
    List<StockOrder> mProductList; // Original Values
    LayoutInflater inflater;
    ViewHolder holder = new ViewHolder();
    List<StockOrder> selectedOrder;
    AppSession mAppSession;


    List<StockOrder> mProductListDisplayed; // Values to be displayed

    public StockProductListAdapter(Context context,
                                   List<StockOrder> data) {

        // TODO Auto-generated constructor stub
        mAppSession = AppSession.getInstance();
        this.mContext = context;
        this.mProductList = data;
        this.mProductListDisplayed = data;
        selectedOrder = new ArrayList<StockOrder>();
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return mProductListDisplayed == null ? 0 : mProductListDisplayed.size();
    }

    @Override
    public Object getItem(int i) {
        return mProductListDisplayed.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        //if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater
                    .inflate(R.layout.stock_product_item_lv, null);
            holder.productName = (TextView) convertView
                    .findViewById(R.id.stock_product_name);
            holder.stockprqty = (EditText) convertView
                    .findViewById(R.id.stock_product_qty_edt);
            holder.prId = (TextView) convertView.findViewById(R.id.stock_pr_id);
            convertView.setTag(holder);
        /*} else {
            holder = (ViewHolder) convertView.getTag();
        }*/
        holder.productName.setText(mProductListDisplayed.get(position).getProductName());
        holder.prId.setText(String.valueOf(mProductListDisplayed.get(position).getProductID()));
        holder.stockprqty.setText("0");



        return convertView;
    }


    private class ViewHolder {
        TextView productName, prId;
        EditText stockprqty;

    }


    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,FilterResults results) {

                mProductListDisplayed = (List<StockOrder>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<StockOrder> FilteredArrList = new ArrayList<StockOrder>();

                if (mProductList == null) {
                    mProductList = new ArrayList<StockOrder>(mProductListDisplayed); // saves the original data in mOriginalValues
                }

                /********
                 *
                 *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
                 *  else does the Filtering and returns FilteredArrList(Filtered)
                 *
                 ********/
                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = mProductList.size();
                    results.values = mProductList;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mProductList.size(); i++) {
                        String data = mProductList.get(i).ProductName;
                        if (data.toLowerCase().contains(constraint.toString()))
                        {
                            StockOrder so = new StockOrder();
                            so.setProductID(mProductList.get(i).getProductID());
                            so.setProductName(mProductList.get(i).getProductName());
                            so.setStockQuantity(mProductList.get(i).getStockQuantity());
                            FilteredArrList.add(so);
                        }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }
}
