package solutions.belinnov.com.stockorder;


import java.util.List;
import java.util.UUID;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by Gaurav on 6/10/2016.
 */
public interface RestInterface {


    @GET("/GetStockTemplates")
    void getStockTemplates(Callback<List<StockTemplate>> response);


    @GET("/GetStockTemplateItems")
    void getStockOrder(@Query("TemplateID") int tid, Callback<List<StockOrder>> response);


}
