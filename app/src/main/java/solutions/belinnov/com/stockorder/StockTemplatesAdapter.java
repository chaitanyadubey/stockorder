package solutions.belinnov.com.stockorder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by belinnove solution on 01/05/2018.
 */

public class StockTemplatesAdapter extends BaseAdapter {
    Context mContext;
    List<StockTemplate> stocksList;
    LayoutInflater inflater;
    ViewHolder holder = new ViewHolder();

    public StockTemplatesAdapter(Context context,
                                 List<StockTemplate> data) {
        // TODO Auto-generated constructor stub
        this.mContext = context;
        this.stocksList = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return stocksList == null ? 0 : stocksList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater
                    .inflate(R.layout.stock_pr_temp_item_lv, null);
            holder.productName = (TextView) convertView
                    .findViewById(R.id.stock_product_temp_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.productName.setText(stocksList.get(position).getTemplateName());
        return convertView;
    }

    private class ViewHolder {
        LinearLayout item_layout;
        TextView productName, target, count, balance;
    }
}