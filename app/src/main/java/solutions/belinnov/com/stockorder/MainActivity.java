package solutions.belinnov.com.stockorder;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    ListView stock_template_lv;
    StockTemplatesAdapter stockProductTemplatesAdapter;
    ProgressBar mProgressBar;
    RestClient restClient;
    RestInterface restInterface;
    AppSession mAppSession;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        restClient = new RestClient();
        restInterface = restClient.getApiService();
        mAppSession = AppSession.getInstance();

        stock_template_lv = (ListView) findViewById(R.id.stock_product_templates_lv);
        mProgressBar = (ProgressBar) findViewById(R.id.stock_layout_progressBar);
        intStockTemplates();
        stock_template_lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                   Intent intent = new Intent(MainActivity.this, StockProductsActivity.class);
                   startActivity(intent);

            }
        });

    }

    private void intStockTemplates() {
        mProgressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            restInterface.getStockTemplates(new Callback<List<StockTemplate>>() {
                @Override
                public void success(List<StockTemplate> stock, Response response) {
                    mProgressBar.setVisibility(View.GONE);
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    mAppSession.setStockProducts(stock);
                    stockProductTemplatesAdapter = new StockTemplatesAdapter(MainActivity.this, mAppSession.getStockProducts());
                    stock_template_lv.setAdapter(stockProductTemplatesAdapter);

                }

                @Override
                public void failure(RetrofitError error) {
                    mProgressBar.setVisibility(View.GONE);
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    displayAlert("Error");
                }
            });
}

    private void displayAlert(String message) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
        // Setting Dialog Title
        alertDialog.setTitle(getResources().getString(R.string.app_name));
        // Setting Dialog Message
        alertDialog
                .setMessage(message);
        // On pressing Settings button
        alertDialog.setPositiveButton(getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        /*finish();
                        System.exit(0);*/
                    }
                });
        // Showing Alert Message
        alertDialog.show();
    }
}
