package solutions.belinnov.com.stockorder;

/**
 * Created by belinnove solution on 01/05/2018.
 */

public class StockTemplate {

    public int TemplateID;
    public String TemplateName;

    public int getTemplateID() {
        return TemplateID;
    }

    public void setTemplateID(int templateID) {
        TemplateID = templateID;
    }

    public String getTemplateName() {
        return TemplateName;
    }

    public void setTemplateName(String templateName) {
        TemplateName = templateName;
    }


}
